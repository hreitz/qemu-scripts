#!/usr/bin/python
# pylint: disable=invalid-name
"""
Takes arguments for dd, scans them for files (if= and of=), and exports
all non-raw disk images with the QEMU storage daemon through FUSE; then
invokes dd (which will thus operate on the raw images).
"""

import json
import os
import subprocess
import sys
from typing import Optional

images = {}

for arg in sys.argv[1:]:
    if arg.startswith('if=') or arg.startswith('of='):
        filename = arg[3:]

        # Ignore non-existing files, the user probably wants to make
        # dd create them (as raw images)
        if not os.path.exists(filename):
            continue

        qemu_img = subprocess.Popen(('qemu-img', 'info', filename),
                                    stdout=subprocess.PIPE,
                                    universal_newlines=True)
        output: str = qemu_img.communicate()[0]
        if qemu_img.returncode != 0:
            sys.exit(os.EX_NOINPUT)

        fmt_line = next(line
                        for line in output.split('\n')
                        if line.startswith('file format: '))
        fmt = fmt_line.split(': ', 1)[1]

        if fmt != 'raw':
            images[filename] = fmt

qsd: Optional[subprocess.Popen] = None

if images:
    args = ['qemu-storage-daemon']

    for i, (image, fmt) in enumerate(images.items()):
        args += ['--blockdev',
                 f'{fmt},node-name=export{i},' +
                 f'file.driver=file,file.filename={image}',
                 '--export',
                 f'fuse,id=fuse{i},node-name=export{i},' +
                 f'mountpoint={image},writable=on']

    args += ['--chardev', 'stdio,id=setup-done',
             '--monitor', 'setup-done']

    qsd = subprocess.Popen(args,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

    assert qsd.stdin is not None
    assert qsd.stdout is not None

    # Once we get a greeting, we know all exports are up
    qmp_line = json.loads(qsd.stdout.readline())
    if 'QMP' not in qmp_line:
        qsd.terminate()
        qsd.wait()
        sys.stderr.write('The QEMU storage daemon did not provide ' +
                         'a QMP monitor\n')
        sys.exit(os.EX_SOFTWARE)

subprocess.Popen(['dd'] + sys.argv[1:]).wait()

if qsd is not None:
    qsd.terminate()
    qsd.wait()
