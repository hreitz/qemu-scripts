#!/usr/bin/python
# pylint: disable=invalid-name
"""
Launch the QEMU storage daemon for the given disk images to make them
accessible as raw images (through FUSE).
"""

import argparse
import json
import os
import signal
import subprocess
import sys

images = {}

argparser = argparse.ArgumentParser(
    description='Access image files in any format understood by QEMU as raw ' +
                'images')

argparser.add_argument('images', metavar='image', type=str, nargs='+',
                       help='a formatted image to present as a raw image')

argparser.add_argument('-d', '--daemonize', action='store_true',
                       help='fork the process and let the parent exit once ' +
                            'the exports are set up')

argparser.add_argument('-p', '--pid-file', type=str,
                       help='store the storage daemon’s PID in the given file')

argparser.add_argument('-q', '--quiet', action='store_true',
                       help='do not print any informational messages')

args = argparser.parse_args()

for image in args.images:
    qemu_img = subprocess.Popen(('qemu-img', 'info', image),
                                stdout=subprocess.PIPE,
                                universal_newlines=True)
    output: str = qemu_img.communicate()[0]
    if qemu_img.returncode != 0:
        sys.exit(os.EX_NOINPUT)

    fmt_line = next(line
                    for line in output.split('\n')
                    if line.startswith('file format: '))
    fmt = fmt_line.split(': ', 1)[1]

    if fmt == 'raw':
        if not args.quiet:
            sys.stderr.write(f'WARNING: "{image}" seems to be a raw image, so '
                             'there is no point in re-exporting it.  ' +
                             'SKIPPED.\n')
    else:
        images[image] = fmt

def terminate_abnormally(msg, qsd_subp):
    """
    Terminate the QSD, print the given error message, and exit with an
    error code.
    """
    qsd_subp.terminate()
    qsd_subp.wait()
    sys.stderr.write(msg + '\n')
    sys.exit(os.EX_SOFTWARE)


qsd_args = ['qemu-storage-daemon']

if args.pid_file is not None:
    qsd_args += ['--pidfile', args.pid_file]

for i, (image, fmt) in enumerate(images.items()):
    qsd_args += ['--blockdev',
                 f'{fmt},node-name=export{i},' +
                 f'file.driver=file,file.filename={image}',
                 '--export',
                 f'fuse,id=fuse{i},node-name=export{i},' +
                 f'mountpoint={image},writable=on']

qsd_args += ['--chardev', 'stdio,id=setup-done',
             '--monitor', 'setup-done']

qsd = subprocess.Popen(qsd_args,
                       stdin=subprocess.PIPE,
                       stdout=subprocess.PIPE,
                       universal_newlines=True)

assert qsd.stdin is not None
assert qsd.stdout is not None

# Once we get a greeting, we know all exports are up
qmp_line = json.loads(qsd.stdout.readline())
if 'QMP' not in qmp_line:
    terminate_abnormally('The QEMU storage daemon did not provide a QMP ' +
                         'monitor', qsd)

if not args.daemonize:
    if not args.quiet:
        print('All exports set up, ^C to revert')

    try:
        qsd.wait()
    except KeyboardInterrupt:
        qsd.terminate()
        qsd.wait()
